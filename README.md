# streamradio
**streamradio** is a command-line interface to play audio streams
available through direct URL access.

Currently the interaction is focused on streams that originate
from [Exclusive Radio](https://exclusive.radio).

Upcoming revisions will add other streams and more customization
to support user preferences.

## Prerequisites
**streamradio** depends on [mpv](https://mpv.io) for command-line 
audio playing. Consult your favorite package manager (e.g., [Homebrew](https://brew.sh) on Mac OS)
for installation. Ensure you can run `mpv` from the command-line.

## Usage
The primary script is `menu.sh`. Consider making it executable (`chmod u+x`) for 
simplicity. Whenever `mpv` is quit `q`, the menu is reloaded. At the menu, use `q` 
to quit **streamradio**.

1. **Basic case:** Use the built in menu `./menu.sh` and pick from one of the artists
by the number in the menu.
2. **Random artist:** Choose `r` from the menu for a random pick. All other artists
go into a random order in a playlist. You can use the `mpv` features to navigate
through artists (`>` for next stream, `<` for previous stream).
3. **Direct selection from start:** Just enter a few letters of the artist. Example: 
`./menu.sh ledz` to immediately play Led Zeppelin.
4. **Custom playlist:** Create a `custom.sh` file and enter some names. There is an
example file loaded in this project. Choose `c` from `menu.sh`. If you choose `d` your custom 
list goes into a playlist that you can navigate within `mpv`.
5. **Soma FM:** Still in progress but use `s` for a playlist of Soma FM stations
6. **Help:** `-h` for various options available


## Future
1. Add more artist sets and genres from [Exclusive Radio](https://exclusive.radio).
2. Add other streams including: Soma FM, (what else?).
3. See if the time of quit during a stream is possible from `mpv` in order to 
keep a local value for a stream where it's linked to a file. This would be a 
way to use as a podcast player.
4. Explore conversion to another language like Python.
