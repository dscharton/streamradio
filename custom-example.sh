#!/bin/bash

# Create a list like this of exclusive.radio listed bands 
# and name it "custom.sh" to use with the (c)ustom option
# in menu.sh.
#
# See menu.sh for a list but there are others
artists=("ledzeppelin" "yes" "u2" "foofighters" "rush" "talkingheads")