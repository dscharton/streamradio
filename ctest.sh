#!/bin/bash

# Zenquote
quote() {
    Q=$(curl --no-progress-meter https://zenquotes.io/api/random | jq '.[]')
    THEQUOTE=$(echo $Q | jq '.' | jq -r '.q')
    THEAUTHOR=$(echo $Q | jq '.' | jq -r '.a')
    printf "\e[31m$THEQUOTE\e[0m -- \e[33m$THEAUTHOR\e[0m\n"
}

httpbin() {
    delay=5
    echo "Starting delay of $delay seconds"
    Q=$(curl -m 2 --no-progress-meter https://httpbin.org/delay/$delay | jq '.[]')
    # THEQUOTE=$(echo $Q | jq '.' | jq -r '.q')
    # THEAUTHOR=$(echo $Q | jq '.' | jq -r '.a')
    # printf "\e[31m$THEQUOTE\e[0m -- \e[33m$THEAUTHOR\e[0m\n"
}
quote
httpbin