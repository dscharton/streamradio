#!/bin/bash

artists=("10cc" "allmanbrothers" "acdc" "aerosmith" "alicecooper" "amywinehouse" "arcticmonkeys"
        "beatles" "billyjoel" "blacksabbath" "blur" "boston" 
        "cheaptrick" "chicago" "chilipeppers" 
        "davidbowie" "defleppard" "deeppurple" "depechemode" "direstraits" "doors" "duranduran" 
        "eagles" "elo" "eltonjohn" "emersonlakepalmer"
        "fleetwoodmac" "foreigner" "foofighters" 
        "genesis" "greenday" "gunsnroses" "ironmaiden"
        "jethrotull" "kingsofleon" "kraftwerk" "ledzeppelin" 
        "manicstreetpreachers" "metallica" "motleycrue" "muse" 
        "nirvana" "oasis" "petergabriel" "pinkfloyd" "police" 
        "queen" 
        "radiohead" "rem" "rock" "rollingstones" "rush" 
        "scorpions" "steelydan" "stonetemplepilots" "supertramp" "stevierayvaughan"
        "talkingheads" "theband" "theblackcrowes" "thecars" "theclash" 
        "thekillers" "thesmiths" "thewho" "tompetty" "toto" twentyonepilots
        "u2" "vanhalen" "velvetunderground" "whitestripes" "yes" "zztop")