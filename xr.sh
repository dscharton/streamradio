#!/bin/zsh

# mpv https://streaming.exclusive.radio/er/$1/icecast.audio

while getopts '1234567a:fg:ln:shxr' arg
do
	case $arg in 
		1) BAND=yes ;;
		2) BAND=genesis ;;
		3) BAND=rush ;;
		4) BAND=rollingstones ;;
		5) BAND=ledzeppelin ;;
		6) BAND=foofighters ;;
		7) BAND=stonetemplepilots;;
		a) ANY=1 ;;
		f) SHUFFLE=1 ;;
		g) FAVS=1 ;;
		l) LS=1 ;;
		n) NEW=1 ;;
		s) SHUFFLE=2 ;;
		x) SOMA=1 ;;
		r) RADIO=1 ;;
		h) 	echo "xr -(number)" 
		 	echo "1) yes" 
			echo "2) genesis"
			echo "3) rush"
			echo "4) rollingstones"
			echo "5) ledzeppelin"
			echo "6) foofighters"
			echo "7) stonetemplepilots"
			echo "a) ANY (substitute name)"
			echo "f) FAVORITES (shuffle)"
			echo "l) FAVORITES (list)"
			echo "g) FAVORITES (start track#)"
			echo "n) ANY (new stations-new.m3u)"
			echo "s) SHUFFLE"
			echo "x) Soma.FM Groove Salad"
			echo "r) Radio Stations"
			HELP=1
			;;
	esac
done


if [[ $SHUFFLE -eq 1 ]]; then
	PLAYLIST=./favorites-new.m3u
	mpv --playlist=$PLAYLIST --shuffle
elif [[ $SHUFFLE -eq 2 ]]; then
	PLAYLIST=./allstations.m3u
	mpv --playlist=$PLAYLIST --shuffle
elif [[ $FAVS -eq 1 ]]; then
	PLAYLIST=./favorites-new.m3u
	# needs to be OPTARG-1
	mpv --playlist=$PLAYLIST --playlist-start=$OPTARG
elif [[ $ANY -eq 1 ]]; then
	STREAM=`grep -i $OPTARG "allstations.m3u" | head -1`
	echo $STREAM
 	mpv $STREAM
elif [[ $NEW -eq 1 ]]; then
	STREAM=`grep -i $OPTARG "favorites-new.m3u" | head -1`
	echo $STREAM
 	mpv $STREAM
elif [[ $LS -eq 1 ]]; then
	cat -n ./favorites.m3u
elif [[ $SOMA -eq 1 ]]; then
	PLAYLIST=./somafm.m3u
	mpv --playlist=$PLAYLIST 
elif [[ $RADIO -eq 1 ]]; then
	PLAYLIST=./radio.m3u
	mpv --playlist=$PLAYLIST 
elif [[ $HELP -eq 1 ]]; then
	exit
else
	STREAM=https://streaming.exclusive.radio/er/$BAND/icecast.audio
	echo "mpv $STREAM"
 	mpv $STREAM
fi
