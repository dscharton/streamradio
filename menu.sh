#!/bin/bash

# Example
# https://streaming.exclusive.radio/er-app/gunsnroses/icecast.audio
# https://streaming.exclusive.radio/er-app/allmanbrothers/icecast.audio
# Source of tutorial
# https://www.baeldung.com/linux/shell-script-simple-select-menu
# Add Zen quotes in lieu of goodbye
# https://zenquotes.io/api/random
# Colors in bash
# https://misc.flogisoft.com/bash/tip_colors_and_formatting
# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux   
PS3="Select your band by # or (h=help c=custom.sh, r=random, a=artist, s=soma, q=quit): "

# choice = the value of the band (e.g. aerosmith)
# REPLY = the value of the input (e.g 1 10 50 q)
. exclusiveradio.sh
# Play direct based on substring of arg 1
playFuzzy() {
    for i in ${artists[@]}
    do 
        if [[ "$i" == *"$1"* ]]; then
            echo "Playing $i"
            mpv https://streaming.exclusive.radio/er-app/$i/icecast.audio
        fi
    done
}

# Create a playlist from a list of artists
playlist() {
    rm "./playlist.m3u"
    touch "./playlist.m3u"
    for item in ${artists[@]}; do
        echo "https://streaming.exclusive.radio/er-app/$item/icecast.audio" >> "./playlist.m3u"
    done
}

somaPlaylist() {
    rm "./soma.m3u"
    touch "./soma.m3u"
    # for item in ${soma[@]}; do
    for item in ${artists[@]}; do
        echo "https://ice4.somafm.com/${item}-128-aac" >> "./soma.m3u"
    done
}

# Zenquote
quote() {
    Q=$(curl --no-progress-meter https://zenquotes.io/api/random | jq '.[]')
    THEQUOTE=$(echo $Q | jq '.' | jq -r '.q')
    THEAUTHOR=$(echo $Q | jq '.' | jq -r '.a')
    printf "\e[31m$THEQUOTE\e[0m -- \e[33m$THEAUTHOR\e[0m\n"
}

help() {
    echo ""
    echo "*************************************************"
    echo "* c = use custom.sh                             *"
    echo "* d = use custom.sh and create an mpv playlist  *"
    echo "* r = random Exclusive Radio                    *"
    echo "* p = playlist of Exclusive Radio               *"
    echo "* a = artist from Exclusive Radio (partial ok)  *"
    echo "* g = Soma FM Groove Salad various streams      *"
    echo "* s = Soma FM stations                          *"
    echo "* q = QUIT                                      *"
    echo "*************************************************"
    echo ""
}

# Check if direct request
if [[ $# -eq 1 ]]; then
    playFuzzy $1
fi


while true; do
    select choice in "${artists[@]}" 
    do
        case $REPLY in 
            z)
                # Hidden option for testing
                quote
                exit 0;;
            q) 
                quote
                # echo "Goodbye! q"
                exit 0;;
            r)
                # MENURANDOM=$((1 + RANDOM % ${#artists[@]}))
                # echo "Picking a random show $MENURANDOM (${artists[MENURANDOM]})"
                echo "Created a random playlist based on artists"
                # ./xr.sh -n ${artists[MENURANDOM]}
                playlist
                mpv "./playlist.m3u" --shuffle
                # echo ${artists[MENURANDOM]}
                break;
                ;;
            a)
                read -p "Enter artist: " a
                playFuzzy $a
                break;;
            c)
                echo "Using custom list in custom.sh"
                . custom.sh
                break;;
            d)
                echo "Using custom playlist in custom.sh"
                . custom.sh
                playlist
                mpv "./playlist.m3u"
                break;;
            e)
                echo "Switching to Exclusive Radio list"
                . exclusiveradio.sh
                playlist
                mpv "./playlist.m3u"
                break;;
            g)
                echo "Soma FM Groove Salad test"
                mpv "./groovesalad.m3u"
                break;;
            h)
                help
                break;;
            s) 
                echo "Soma FM Playlist"
                SOMAFM=1
                . soma.sh
                somaPlaylist
                break;;
        esac

        case $choice in
            *)
                let startat=$REPLY-1
                if [[ SOMAFM -eq 1 ]]; then
                    mpv "./soma.m3u" --playlist-start=$startat
                    echo "SOMA"
                    break;
                else
                    # Create a playlist and start at 0-based index in the playlist
                    playlist
                    mpv "./playlist.m3u" --playlist-start=$startat
                    break;
                fi
        esac
    done
done