#!/bin/bash

BAND="rock"
if [[ $# -eq 1 ]]; then
    BAND=$1
fi

mpv https://streaming.exclusive.radio/er/$BAND/icecast.audio
